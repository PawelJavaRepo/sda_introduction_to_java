package zajecia2.instrukcjesterujace;

/* BMI = Waga [kg] / wzrost x wzrost [cm]*/

import java.util.Scanner;

public class BMI {

    public static void main(String[] args) {

        int kg;
        int cm;

        Scanner odczyt = new Scanner(System.in);

        System.out.println("Poniżej wpisz swoją wagę wyrażoną w kilogramach.");

        kg = odczyt.nextInt();

        System.out.println("Poniżej wpisz swój wzrost wyrażony w centymetrach.");

        cm = odczyt.nextInt();

        float wzrostm = cm/100f;
        float bmi = kg / (wzrostm * wzrostm);

        if (bmi < 18) {
            System.out.println("Twoje BMI wynosi " + bmi + " Masz niedowagę!");
        } else if  (bmi > 18 && bmi <=24) {
            System.out.println("Twoje BMI wynosi " + bmi + ". Waga właściwa ;)");
        } else if (bmi > 24 && bmi <= 29) {
            System.out.println("Twoje BMI wynosi " + bmi + ". Masz nadwagę!");
        } else if (bmi > 29 && bmi <= 39) {
            System.out.println("Twoje BMI wynosi " + bmi + ". Otyłość!!!");
        } else {
            System.out.println("Twoje BMI wynosi " + bmi + ". Poważna OTYŁOŚĆ!!!");
        }

    }
}
