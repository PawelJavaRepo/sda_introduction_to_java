package zajecia2.instrukcjesterujace;

import java.util.Scanner;

public class RownanieKwadratowe {
    public static void main(String[] args) {
        int a;
        int b;
        int c;
        System.out.println("Podaj wartosci wspolczynnikow a b c");

        //pobierz wartosci wspolczynnikow
        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj a:");
        a = odczyt.nextInt();
        System.out.println("Podaj b:");
        b = odczyt.nextInt();
        System.out.println("Podaj c: ");
        c = odczyt.nextInt();

        //oblicz wartosc delta ze wzoru
        int delta = b * b - 4 * a * c;
        //ustal liczbe rozwiazan:
        if (delta < 0) {
            System.out.println("Brak rozwiazan w zbiorze liczb rzeczywistych");
        } else if (delta == 0) {
            double wynik = (double) -b / (2 * a);
            System.out.println("Jedno rozwiazanie: " + wynik);
        } else {
            double pierwiastek = Math.sqrt(delta);
            double x1 = (-b - pierwiastek) / (2 * a);
            double x2 = (-b + pierwiastek) / (2 * a);
            System.out.println("Dwa rozwiazania :" + x1 + " " + x2);
        }

    }
}
