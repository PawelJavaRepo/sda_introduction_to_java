package zajecia2.kalkulator;

/*              Stwórz kalkulator który pobierze dwie wartości
*               następnie zapyta co z nimi zrobić i wykona to działanie*/

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {

        double a;
        double b;

        Scanner odczyt = new Scanner(System.in);
        System.out.println("podaj wartości a i b");

        a = odczyt.nextDouble();

        System.out.println("podaj wartość b");
        b = odczyt.nextDouble();

        System.out.println("---------------------------------------------------");
        System.out.println("Wybierz opcje: ");
        System.out.println("1.  Dodawanie");
        System.out.println("2.  Odejmowanie");
        System.out.println("3.  Mnożenie");
        System.out.println("4.  Dzielenie");

        int wybor = odczyt.nextInt();

        switch (wybor) {
            case 1:
                System.out.println(a+b);
                break;
            case 2:
                System.out.println(a-b);
                break;
            case 3:
                System.out.println(a*b);
                break;
            case 4:
                System.out.println(a/b);
                break;
            default:
                System.out.println("Wybór nie mieści mi się w RAMie");
                break;
        }

    }
}
