package zajecia2.konwerter;

import java.util.Scanner;

/*          Program służący do konwersji wartości temperatury podanej w stopniach Celsjusza na stopnie w skali Fahrenheita
                        st F = 1.8 * st C + 32.0*/
public class Konwerter {
    public static void main(String[] args) {

        double celsjus;

        Scanner odczyt = new Scanner(System.in);

        System.out.println("Podaj temperaturę w stopniach Celsjusza.");
        celsjus = odczyt.nextDouble();

//        obliczam konwersję

        double konwersja = celsjus * 1.8 + 32;

        System.out.println("Temperatura wynosi " + celsjus + " stopni Celsjusza, a to się równa " + konwersja + " stopniom Fahrenheita");

//        camelCase - pisanie na wielbłąda
//        rzutowanie    -   jawna konwersja typu danych

    }
}
