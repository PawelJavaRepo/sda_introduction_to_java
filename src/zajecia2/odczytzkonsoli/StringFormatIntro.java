package zajecia2.odczytzkonsoli;

public class StringFormatIntro {

    public static void main(String[] args) {

        String imie = "Paweł";
        String nazwisko = "Sydorowicz";

        System.out.println("Mam na imie " + imie + " a na nazwisko " + nazwisko);

        String kom = String.format("Mam na imie %s a na nazwisko %s", imie, nazwisko);
        System.out.println(kom);
    }
}
