package zajecia2.delta;

import java.util.Scanner;

public class ObliczDelte {

    public static void main(String[] args) {

        int a;
        int b;
        int c;
        System.out.println("Podaj wartości współczynników a, b i c");

//        pobierz wartości współczynników
        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj a:");
        a = odczyt.nextInt();
        System.out.println("Podaj b:");
        b = odczyt.nextInt();
        System.out.println("Podaj c:");
        c = odczyt.nextInt();

//        oblicz wartosci delta ze wzoru
        int delta = b * b - 4 * a * c;
//        ustal liczbe rozwiazan
        if (delta<0) {
            System.out.println("Brak rozwiązań w zbiorze liczb rzeczywistych");
        } else if (delta == 0) {
            double wynik = (double) -b / (2 * a);
            System.out.println("Jedno rozwiązanie " + wynik);
        } else {
            double pierwiastek = Math.sqrt(delta);
            double x1 = (-b - pierwiastek) / (2 * a);
            double x2 = (-b + pierwiastek) / (2 * a);
            System.out.println("Dwa rozwiązania: " + x1 + " " + x2);
        }
//        oblicz pierwiastki
    }
}
