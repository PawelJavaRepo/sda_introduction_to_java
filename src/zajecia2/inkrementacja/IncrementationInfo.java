package zajecia2.inkrementacja;

public class IncrementationInfo {
    public static void main(String[] args) {
        int wiek = 10;                  // deklaracja i inicjalizacja zmiennej
        wiek = wiek + 1;                // pierwszy sposob inkrementacji
        wiek += 1;                      // drugi sposób inkrementacji
        wiek++;                         // najlepszy sposób na inkrementację +1

    }
}
