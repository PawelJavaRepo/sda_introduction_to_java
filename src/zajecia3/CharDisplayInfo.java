package zajecia3;

/*      Wyswietl znaki od A do Z z uzyciem petli FOR        */
public class CharDisplayInfo {
    public static void main(String[] args) {

        for (char c = 'A'; c <= 'Z'; c++) {
            System.out.println("Pozycja: " + (int)c + " znak: " + c);
        }

        char a = '%';
        System.out.println((short)a);
    }
}
