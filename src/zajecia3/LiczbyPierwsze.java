package zajecia3;

/*      Wyświetlić​ ​ na​ ​ ekranie​ ​ liczby​ ​ całkowite​ ​ należące​ ​ do​ ​ przedziału​ ​ (100,​ ​ 300)​ ​
oraz​ ​ korzystając​ ​ z operatora​ ​ trójargumentowego​ ​ wypisać​ ​ komunikat​ ​ czy​ ​ dana​ ​ liczba​ ​
jest​ ​ liczbą​ ​ parzystą​ ​ czy nieparzystą. */

import java.util.Scanner;

public class LiczbyPierwsze {

    public static boolean isPrime(int number) {
        boolean result = true;

        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                result = false;
                break;
            }
        }

        return result;

    }

    public static void main(String[] args) {
        System.out.println("podaj liczbe");
        Scanner sc = new Scanner(System.in);
        int liczba = sc.nextInt();
        boolean wynik = isPrime(liczba);
        System.out.println("Czy pierwsza: " + liczba + " = " + wynik);
    }
}
