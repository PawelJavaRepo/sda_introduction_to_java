package zajecia3;
/*  Pętla While drukująca liczby od 0 - 100 */

import java.util.Scanner;

public class PetleWhileCiagDalszy {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
//        zapytac uzytkownika o zakres liczb
        System.out.println("Podaj zakres liczb:");

        int a = sc.nextInt();
        int b = sc.nextInt();

        // wypisać parzyste z tego zakresu

        while (a <= b) {
            if (a % 2 == 0) {
                System.out.println(a);
            }
            a++;
        }
    }
}


