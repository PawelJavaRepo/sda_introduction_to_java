package zajecia3;
/*      Program który przy uzyciu instrukcji for sumuje liczby calkowite od 1 do 100        */
public class Zadanie3 {
    public static void main(String[] args) {

        int suma = 0;
        for (int i = 1; i <=100; i++) {
            suma += i;
        }
        System.out.println("Suma to: " + suma);
    }
}
