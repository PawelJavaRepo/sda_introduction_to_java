package Zajecia6.Zadanie4;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Siemandero!!!");
        System.out.println("Podaj nazwę firmy: ");
        String name = sc.nextLine();
        Company myCompany = new Company(name);
        printMenu();
        int wybor = sc.nextInt();
        switch (wybor) {
            case 1:
                addNewEmp(sc, myCompany);
                break;
            case 2:
                for (Worker e : myCompany.getEmployees()) {
                    System.out.println(e.getDescription);
                }
                break;
            default:

        }
    }

    public static void addNewEmp(Scanner sc, Company myCompany) {
        System.out.println("podaj imie: ");
        String empName = sc.nextLine();
        System.out.println("Podaj nazwisko: ");
        String empSurname = sc.nextLine();
        System.out.println("Podaj pensje: ");
        double salary = sc.nextDouble();

        Worker employee = new Worker(empName, empSurname, salary);
        boolean isSuccess = myCompany.addEmployee(employee);
        if (isSuccess){
            System.out.println("Dodano pracownika.");
        }else{
            System.out.println("Nie udało się dodać. Zbyt dużo pracowników.");
        }
    }

    public static void printMenu() {
        System.out.println("Wybierz opcje: ");
        System.out.println("1. Dodawanie pracownika: ");
        System.out.println("2. Wyświetlanie pracowników: ");
        System.out.println("Wybór");
    }
}
