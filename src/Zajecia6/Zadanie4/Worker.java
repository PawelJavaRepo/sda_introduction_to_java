package Zajecia6.Zadanie4;

public class Worker {

//    imie, nazwisko, wiek, email, salary

    private String name;
    private String surname;
    private int age;
    private String email;
    private double salary;
    public String getDescription;

    public Worker(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Worker(String name, String surname, double salary) {
        this(name, surname);
        this.salary = salary;
    }

    public Worker(String name, String surname, double salary, String email) {
        this(name, surname, salary);
        this.email = email;
    }

    public Worker(String name, String surname, double salary, String email, int age) {
        this(name, surname, salary, email);
        this.age = age;
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getSurname(){
        return surname;
    }
    public void setSurname(String surname){
        this.surname = surname;
    }
    public int getAge(){
        return age;
    }
    public void setAge(int age){
        this.age = age;
    }
    public String getEmail(){
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public double getSalary(){
        return salary;
    }
    public void setSalary(double salary){
        this.salary = salary;
    }

    public String getDescription() {
        return String.format("Name: %s, Surname: %s, Salary: %f", name, surname, salary);
    }
}
