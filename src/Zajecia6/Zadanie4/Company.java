package Zajecia6.Zadanie4;

public class Company {
    private static final int DEFAULT_SIZE = 50;
    private String name;
    private Worker [] employees;
    private int companySize = 0;

    public Company(String name) {
        this.name = name;
        this.employees = new Worker[DEFAULT_SIZE];

    }

    public Company(String name, int initialSize) {
        this.name = name;
        this.employees = new Worker[initialSize];
    }

    public boolean addEmployee(Worker emp) {
        if(companySize<employees.length){
            employees[companySize++] = emp;
            return true;
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Worker[] getEmployees() {
        return employees;
    }
}
