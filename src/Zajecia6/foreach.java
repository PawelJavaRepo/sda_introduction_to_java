package Zajecia6;

public class foreach {
    public static void main(String[] args) {
        int tab[] = new int[]{1, 2, 3, 4, 5, 6};

        // petla for
        for (int i = 0; i < tab.length; i++) {
            System.out.println(tab[i]);
        }

        //petla for
        System.out.println();
        System.out.println("Użycie pętli foreach");

        for (int elem : tab) {
            elem *= 2;
        }

        for(int elem : tab) {
            System.out.println(elem);
        }

        System.out.println(" ");
    }
}
