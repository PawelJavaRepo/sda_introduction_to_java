package Zajecia6.Zadanie1;

public class Prostokat {
    //    pola do opisywania wlasciwosci prostokata
    private double a;
    private double b;

    //    getter i setter dla pola a
    public double getA() {
        return a;
    }

    public void setA(double a) {
        if (a <= 0) {
            throw new IllegalArgumentException("Długość mniejsza od 0");
        }
        this.a = a;
    }

    //    getter i setter dla pola B
    public double getB() {
        return b;
    }

    public void setB(double b) {
        if (b <= 0) {
            throw new IllegalArgumentException("Długość mniejsza lub równa 0");
        }
        this.b = b;
    }

    //    metoda obliczajaca pole
    public double field() {

        return a * b;
    }

    //        metoda obliczajaca obwod
    public double obwod() {
        return (2 * a) + (2 * b);

    }
}
