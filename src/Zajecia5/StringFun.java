package Zajecia5;

import java.util.Scanner;

public class StringFun {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Wpisz dowolny ciąg znaków. Postaraj zmieścić się w 20 ;) ");
        String text = sc.nextLine();
        if (text.length() <= 20) {
            printMenu();
            int choice = sc.nextInt();
            switch (choice) {
                case 1:
                    toUpper(text);
                    break;
                case 2:
                    toLower(text);
                    break;
                case 3:
                    toggleCase(text);
                    break;
                case 4:
                    System.out.println("Żegnam ozięble!!!");
                    break;
            }
        } else {
            System.out.println("Lancuch znakow zbyt dlugi");
        }

    }

    public static void printMenu() {
        System.out.println("1.  Wszystkie litery na duze.");
        System.out.println("2.  Wszystkie litery na male.");
        System.out.println("3.  Male na duze, a duze na male.");
        System.out.println("4.  BYE");
    }

    public static void toUpper(String input) {
        String result = "";
        for (int i = 0; i < input.length(); i++) {
            char element = input.charAt(i);
            if (element >= 97 && element <= 122) {
                element -= 32;
            }
            result += element;
        }
        System.out.println(result);
    }

    public static void toLower(String input) {
        String result = "";
        for (int i = 0; i < input.length(); i++) {
            char element = input.charAt(i);
            if (element >= 65 && element <= 96) {
                element += 32;
            }
            result += element;
        }
        System.out.println(result);
    }


    public static void toggleCase(String input) {
        String result = "";
        for (int i = 0; i < input.length(); i++) {
            char element = input.charAt(i);
            if (element >= 97 && element <= 122) {
                element -= 32;
            } else if (element >= 65 && element <= 96) {
                element += 32;
            }
            result += element;
        }
        System.out.println(result);
    }

}
