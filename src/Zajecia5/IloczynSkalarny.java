package Zajecia5;
/*          Zainicjalizuj dwa wektory o długości
            wartościami losowymi z przedziału
            〈1;30〉. Oblicz ich iloczyn skalarny
            oraz wyświetl całe działanie na ekranie.
            Wartość n wczytaj od użytkownika.
            */

import java.util.Random;

public class IloczynSkalarny {


//    OKRESLAMY ROZMIAR TABLICY
    public static final int ARRAY_SIZE = 50;

//    OKRESLAMY GORNE OGRANICZENIE GENERATORA LICZB PSEUDOLOSOWYCH
    public static final int RANDOM_UPPER_BOUND = 30;

    public static void main(String[] args) {

        Random rd = new Random();
        int[] wektorA = new int[ARRAY_SIZE];
        int[] wektorB = new int[ARRAY_SIZE];


        for (int i = 0; i < wektorA.length; i++) {
            wektorA[i] = rd.nextInt(RANDOM_UPPER_BOUND);
        }
        for (int i = 0; i < wektorB.length; i++) {
            wektorB[i] = rd.nextInt(RANDOM_UPPER_BOUND);
        }

//      OBLICZANIE WARTOSCI

        int suma = 0;
        for(int i=0;i<ARRAY_SIZE;i++) {
            suma += wektorA[i] * wektorB[i];
        }
        System.out.println(suma);

    }
}
