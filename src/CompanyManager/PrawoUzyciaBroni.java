package CompanyManager;

public class PrawoUzyciaBroni {

    public static void main(String[] args) {

        String wstep = "Broni palnej można użyć, gdy zaistnieje co najmniej jeden z następujących przypadków:";
        String podstawaPrawna = "Podstawa prawna:\n" +
                "\n" +
                "1) Ustawa z dnia 24 sierpnia 2001 r. o Żandarmerii Wojskowej i wojskowych organach porządkowych " +
                "(Dz. U. z 2016 r.  poz. 1483 tekst ujednolicony);\n" +
                "2) Ustawa z dnia 24 maja 2013 r. o środkach przymusu bezpośredniego i broni palnej " +
                "(Dz. U. z 2013 r. nr 0 poz. 628).\n" +
                "Zasady użycia i wykorzystania broni (ustawa z dnia 24 sierpnia 2001 r. o Żandarmerii Wojskowej i " +
                "wojskowych organach porządkowych – Dz. U. Nr 123, poz. 1353 z późn. zm.), " +
                "które obowiązują od dnia 5 czerwca 2013 r. na mocy art. 68 i art. 85 ustawy z dnia 24 maja 2013 r. " +
                "o środkach przymusu bezpośredniego i broni palnej (Dz. U. z 2013 r. poz. 628).";
        String punktPierwszy = "konieczność odparcia bezpośredniego, bezprawnego zamachu na";
        String punktDrugi = "konieczność przeciwstawienia się osobie:";
        String punktTrzeci = "bezpośredni pościg za osobą, wobec której:";
        String punktCzwarty = "konieczności ujęcia osoby, która:";
        String ponadto = "Ponadto broń palna może być wykorzystana, a przez wykorzystanie broni należy rozumieć oddanie strzału " +
                "z zastosowaniem amunicji penetracyjnej w kierunku zwierzęcia, przedmiotu lub w innym kierunku niestwarzającym " +
                "zagrożenia dla osoby w przypadku konieczności podjęcia co najmniej jednego z następujących działań:\n" +
                "1) zatrzymanie pojazdu, jeżeli jego działanie zagraża życiu lub zdrowiu uprawnionego lub innej osoby lub stwarza" +
                " zagrożenie dla ważnych obiektów, urządzeń lub obszarów;\n" +
                "2) pokonanie przeszkody:\n" +
                "     a) uniemożliwiającej lub utrudniającej ujęcie osoby albo ratowanie życia lub zdrowia uprawnionego, " +
                "innej osoby lub  ratowanie mienia,\n" +
                "     b) w przypadku naruszenia porządku lub bezpieczeństwa publicznego przez osobę pozbawioną wolności, " +
                "zatrzymaną lub umieszczoną w strzeżonym ośrodku albo areszcie w celu wydalenia;\n" +
                "3) zaalarmowanie lub wezwanie pomocy;\n" +
                "4) unieszkodliwienie zwierzęcia, którego zachowanie zagraża bezpośrednio życiu lub zdrowiu uprawnionego " +
                "lub innej osoby;\n" +
                "5) oddanie strzału ostrzegawczego.";
        String podpunktA = "życie, zdrowie lub wolność uprawnionego lub innej osoby " +
                "albo konieczność przeciwdziałania czynnościom zmierzającym bezpośrednio " +
                "do takiego zamachu,";
        String podpunktB = "ważne obiekty, urządzenia lub obszary albo konieczność " +
                "przeciwdziałania czynnościom zmierzającym bezpośrednio do takiego zamachu,";
        String podpunktC = "mienie, który stwarza jednocześnie bezpośrednie zagrożenie " +
                "życia, zdrowia lub wolności uprawnionego lub innej osoby, albo " +
                "konieczność przeciwdziałania czynnościom zmierzającym bezpośrednio " +
                "do takiego zamachu,";
        String podpunktD = "nienaruszalność granicy państwowej przez osobę, " +
                "która wymusza przekroczenie granicy państowoej przy użyciu pojazdu, " +
                "broni palnej lub innego niebezpiecznego przedmiotu,";
        String podpunktE = "bezpieczeństwo konwoju lub doprowadzenia,";
        String podpunktA2 = "niepodporządkowującej się wezwaniu do natychmiastowego " +
                "porzucenia broni, materiału wybuchowego lub innego niebezpiecznego " +
                "przedmiotu, którego użycie może zagrozić życiu, zdrowiu lub wolności " +
                "uprawnionego lub innej osoby,";
        String podpunktB2 = "która usiłuje bezprawnie odebrać broń palną " +
                "uprawnionemu lub innej osobie uprawnionej do jej posiadania;";
        String podpunktA3 = "użycie broni palnej było dopuszczalne w przypadkach:";
        String zamachuNa = "zamachu na";
        String koniecznosc = "koniecznosc przeciwstawienia sie osobie ";
        String podejrzenie = "istnieje uzasadnione podejrzenie, ze popelnila przestepstwo";
        String podpunktE4 = "ujęcia lub udaremnienia ucieczki osoby zatrzymanej, " +
                "tymczasowo aresztowanej lub odbywającej karę pozbawienia wolności, " +
                "jeżeli:\n" +
                "    \t- ucieczka tej osoby stwarza zagrożenie życia lub zdrowia " +
                "uprawnionego lub innej osoby,\n" +
                "    \t- istnieje uzasadnione podejrzenie, że osoba ta może użyć " +
                "materiałów wybuchowych, broni palnej lub innego niebezpiecznego " +
                "przedmiotu,\n" +
                "    \t- pozbawienie wolności nastąpiło w związku z uzasadnionym " +
                "podejrzeniem lub stwierdzeniem popełnienia przestępstwa, " +
                "o którym mowa w pkt 3 lit. b.";


        System.out.println(podstawaPrawna);
        System.out.println(" ");
        System.out.println(wstep);
        System.out.println("1) " + punktPierwszy + ":");
        System.out.println("    a) " + podpunktA);
        System.out.println("    b) " + podpunktB);
        System.out.println("    c) " + podpunktC);
        System.out.println("    d) " + podpunktD);
        System.out.println("    e) " + podpunktE);
        System.out.println("2) " + punktDrugi);
        System.out.println("    a) " + podpunktA2);
        System.out.println("    b) " + podpunktB2);
        System.out.println("3) " + punktTrzeci);
        System.out.println("    a) " + podpunktA3);
        System.out.println("        - " + punktPierwszy + " " + podpunktA);
        System.out.println("        - " + zamachuNa + podpunktB);
        System.out.println("        - " + zamachuNa + podpunktC);
        System.out.println("        - " + zamachuNa + podpunktD);
        System.out.println("        - " + koniecznosc + podpunktA2 + " oraz osobie " + podpunktB2);
        System.out.println("    b) " + podejrzenie + " o charakterze:");
        System.out.println("        - terrorystycznym,");
        System.out.println("        - zabojstwa,");
        System.out.println("        - spowodowanie ciężkiego uszczerbku na zdrowiu,");
        System.out.println("        - sprowadzenie zdarzenia lub niebezpieczeństwo " +
                                    "zdarzenia zagrażającego życiu lub zdrowiu wielu osób albo" +
                                    " mieniu w wielkich rozmiarach,");
        System.out.println("        - zgwałcenia,");
        System.out.println("        - wzięcia lub przetrzymywanie zakładnika,");
        System.out.println("        - rozboju, kradzieży rozbójniczej lub wymuszenia rozbójniczego.");
        System.out.println("4. " + punktCzwarty);
        System.out.println("    a) dokonała " + zamachuNa + ":");
        System.out.println("        - " + podpunktA);
        System.out.println("        - " + podpunktB);
        System.out.println("        - " + podpunktC);
        System.out.println("        - " + podpunktD);
        System.out.println("    b) " + podpunktA2);
        System.out.println("    c) " + podpunktB2);
        System.out.println("    d) wobec ktorej " + podejrzenie + " wskazane w pkt 3 lit. b.");
        System.out.println("    e) " + podpunktE4);
        System.out.println(" ");
        System.out.println(ponadto);
    }
}


