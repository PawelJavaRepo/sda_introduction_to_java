package CompanyManager;

public class Soldier {

    private String range;
    private String name;
    private String surname;
    private String position;
    private String birthDate;
    private double salary;
    private String startDate;

    public Soldier(String range, String name, String surname) {
        this.range = range;
        this.name = name;
        this.surname = surname;
    }

    public Soldier(String range, String name, String surname, String position, String birthDate, double salary, String startDate) {
        this (range, name, surname);
        this.position = position;
        this.birthDate = birthDate;
        this.salary = salary;
        this.startDate = startDate;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getDescription() {
        return String.format("Range: %s, Name: %s, Surname: %s, Position: %s, BirthDate: %s, Salary: %f, StartDate: %s");
    }

    @Override
    public String toString() {
        return String.format("%s %s %s %s %s %.2f %s",
                range,
                name,
                surname,
                position,
                birthDate,
                salary,
                startDate
        );
    }
}
