package CompanyManager.fileOperation.writer;

import CompanyManager.Soldier;

public abstract class JsonSoldierWriter extends AbstractSoldierWriter {
    public JsonSoldierWriter(String pathToFile) {
        super (pathToFile);
    }

    @Override
    public Soldier[] readSoldiers(){
        return new Soldier[0];
    }
}
