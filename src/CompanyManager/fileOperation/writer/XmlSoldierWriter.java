package CompanyManager.fileOperation.writer;

import CompanyManager.Company;
import CompanyManager.Soldier;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

public abstract class XmlSoldierWriter extends AbstractSoldierWriter {
    public XmlSoldierWriter(String pathToFile) {
        super(pathToFile);
    }

    @Override
    public void writeSoldiers(Soldier[] soldiers) {
        Company companyToSave = new Company("");
        companyToSave.setSoldiers(soldiers);
        try {
            JAXBContext context = JAXBContext.newInstance(Company.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(companyToSave, new File(pathToFile));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

   /* @Override
    public Soldier[] readSoldiers() {
        return new Soldier[0];
    }*/
}
