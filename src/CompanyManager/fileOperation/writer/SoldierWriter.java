package CompanyManager.fileOperation.writer;

import CompanyManager.Soldier;

public interface SoldierWriter {

    void writeSoldiers(Soldier[] copy);
}
