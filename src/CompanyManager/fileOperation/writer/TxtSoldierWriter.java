package CompanyManager.fileOperation.writer;

import CompanyManager.Soldier;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class TxtSoldierWriter extends AbstractSoldierWriter {
    public TxtSoldierWriter(String pathToFile) {
        super(pathToFile);
    }

    @Override
    public void writeSoldiers(Soldier[] soldiers){
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(pathToFile))){
            bufferedWriter.write(soldiers.length + "\n");
            for (int i=0; i<soldiers.length; i++) {
                Soldier sdr = soldiers[i];
                bufferedWriter.write(sdr.toString() + "\n");
            }
        }catch (IOException s) {
            s.printStackTrace();
        }
    }

    @Override
    public Soldier[] readSoldiers() {
        return new Soldier[0];
    }
}
