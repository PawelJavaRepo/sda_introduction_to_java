package CompanyManager.fileOperation.writer;

import CompanyManager.Soldier;
import CompanyManager.fileOperation.reader.AbstractSoldierReader;

public abstract class AbstractSoldierWriter implements SoldierWriter {

    protected String pathToFile;

    protected AbstractSoldierWriter(String pathToFile) {
        this.pathToFile = pathToFile;
    }

    @Override
    public abstract void writeSoldiers(Soldier[] soldiers);

    public abstract Soldier[] readSoldiers();
}
