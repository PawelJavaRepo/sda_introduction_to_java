package CompanyManager.fileOperation.util;
/*
* KLASA NARZEDZIOWA POMAGAJACA W KONWERSJI
* */
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
// metoda konwertujaca double ze ze String
// pomocna gdy liczba jest zapisana z przecinkiem - jak w Polsce lub we Francji
public class ParseUtils {
    public static double parseDouble(String s) {
        NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
        Number parse = null;
        try {
            parse = format.parse(s);
        }catch(ParseException e) {
            e.printStackTrace();
        }
        return parse.doubleValue();
    }
}
