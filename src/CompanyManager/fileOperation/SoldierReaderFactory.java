package CompanyManager.fileOperation;

import CompanyManager.fileOperation.reader.*;

public class SoldierReaderFactory {
    public static SoldierReader createReader(String path) {
        if (path.endsWith(".txt")){
            return new TxtSoldierReader(path);
        }else if (path.endsWith(".xml")) {
            return new XmlSoldierReader(path);
        }else if(path.endsWith(".json")){
            return new JsonSoldierReader(path);
        }else if (path.endsWith(".csv")){
            return new CsvSoldierReader(path);
        }
        return null;
    }
}
