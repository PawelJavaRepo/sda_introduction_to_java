package CompanyManager.fileOperation.reader;

import CompanyManager.Soldier;

public class JsonSoldierReader extends AbstractSoldierReader {
    public JsonSoldierReader(String pathToFile) {
        super (pathToFile);
    }

    @Override
    public Soldier[] readSoldiers() {
        return new Soldier[0];
    }
}
