package CompanyManager.fileOperation.reader;

import CompanyManager.Soldier;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import static CompanyManager.fileOperation.util.ParseUtils.parseDouble;

//----------------------------------------------------------------------------------------

public class TxtSoldierReader extends AbstractSoldierReader {
    private String range;
    private String name;
    private String surname;
    private String position;
    private int pesel;
    private String birthDate;
    private double salary;
    private String startDate;

    public TxtSoldierReader(String pathToFile) {
        super(pathToFile);
    }

    @Override
    public Soldier[] readSoldiers() {
        Soldier[] soldiers = null;
        int i = 0;
//        try with sources
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(pathToFile))) {
//            odczyt ilosci pracownikow (zakladamy, ze liczba jest w pierwszej linii)
            String companySize = bufferedReader.readLine();
//            utworz tablice dla pracownikow z pliku
//            odczytana ilosc bedzie typu string
//            wiec musimy ja zparsowac, czyli przekonwrtowac typ String na int
//            parsowanie - konwersja typu String
            soldiers = new Soldier[Integer.parseInt(companySize)];
//            odczyt wszystkich linii z pliku
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
//                pobierz 1 linie i podziel po separatorze (u nas to spacja)
                String[] split = line.split(" ");
//           wg struktury id stopien imie nazwisko stanowisko pesel dataUr pensja dataRozpSluzby
                Soldier sdr = new Soldier(range, name, surname, position, birthDate, salary, startDate);
//           ustawiamy odpowiednie pola wg zalozonego schematu
                sdr.setRange(split[0]);
                sdr.setName(split[1]);
                sdr.setSurname(split[2]);
                sdr.setPosition(split[3]);
                sdr.setBirthDate(split[4]);
                sdr.setSalary(parseDouble(split[5]));
                sdr.setStartDate(split[6]);
//                wpisujemy utworzonego pracownika do tablicy pracownikow
                soldiers[i++] = sdr;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        zwracamy tablice pracownikow
        return soldiers;
    }
}
