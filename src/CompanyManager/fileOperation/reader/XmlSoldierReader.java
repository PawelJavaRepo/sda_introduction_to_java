package CompanyManager.fileOperation.reader;

import CompanyManager.Company;
import CompanyManager.Soldier;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.jar.JarException;

public class XmlSoldierReader extends AbstractSoldierReader {
    public XmlSoldierReader(String pathToFile) {
        super(pathToFile);
    }


    @Override
    public Soldier[] readSoldiers() throws JAXBException {
        try {
            JAXBContext context = JAXBContext.newInstance(Company.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Object obj = unmarshaller.unmarshal(new File(pathToFile));
            Company result = (Company) obj;
            return result.getSoldiers();
        } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }
    }
}
