package CompanyManager.fileOperation.reader;

import CompanyManager.Soldier;

import javax.xml.bind.JAXBException;

public interface SoldierReader {
    Soldier[] readSoldiers() throws JAXBException;
}
