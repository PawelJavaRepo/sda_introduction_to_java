package CompanyManager.fileOperation.reader;

import CompanyManager.Soldier;

import javax.xml.bind.JAXBException;

/*
* Abstrakcyjny czytnik - jedyne co możemy o nim powiedziec to ze bedziemy czytac z pliku
* wiec tutaj przechowujemy sciezke do pliku
* */

public abstract class AbstractSoldierReader implements SoldierReader {
    protected String pathToFile;

    protected AbstractSoldierReader(String pathToFile) {
        this.pathToFile = pathToFile;
    }

    public abstract Soldier[] readSoldiers() throws JAXBException;
}
