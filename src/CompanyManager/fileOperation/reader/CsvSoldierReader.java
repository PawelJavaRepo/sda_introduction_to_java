package CompanyManager.fileOperation.reader;

import CompanyManager.Soldier;

public class CsvSoldierReader extends AbstractSoldierReader {
    public CsvSoldierReader(String pathToFile){
        super(pathToFile);
    }

    @Override
    public Soldier[] readSoldiers(){
        return new Soldier[0];
    }
}
