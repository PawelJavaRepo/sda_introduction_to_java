package Zajecia4;

import java.util.Scanner;

public class FibonacciExample {
    public static void main(String[] args) {
//        pobrać n z klawiatury
//        wywolac funkcje fibonacci z klasy math.helper
//        wydrukowac wynik

        Scanner sc = new Scanner(System.in);
        int n;
        System.out.println("Podaj dowolną liczbę całkowitą: ");
        n = sc.nextInt();
        int fib = MathHelper.fibonacci(n);

    }
}
