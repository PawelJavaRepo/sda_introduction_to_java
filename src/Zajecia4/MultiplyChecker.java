package zajecia4;

import java.util.Random;
import java.util.Scanner;

public class MultiplyChecker {
    public static void main(String[] args) {
        boolean czyPoprawnaOdpowiedz = false;
        System.out.println("Losuje dwie liczby... Podaj wynik ich mnozenia");
        Scanner sc = new Scanner(System.in);
        Random rd = new Random();


        while (czyPoprawnaOdpowiedz == false) {
            int liczba1 = rd.nextInt(10);
            int liczba2 = rd.nextInt(10);
            System.out.println("Podaj wynik mnozenia: " + liczba1 + " * " + liczba2);
            int wynik = sc.nextInt();
            if (wynik == liczba1 * liczba2) {
                System.out.println("Poprawna odpowiedz - wygrales");
                czyPoprawnaOdpowiedz = true;
            } else {
                System.out.println("Bledna odpowiedz");
            }
        }
        System.out.println("Koniec pracy");
    }
}
