package zajecia4;

import java.util.Scanner;

public class ArrayIntro {
    public static void main(String[] args) {
        //Utworzenie tablicy 10 - elementowej
        int[] tab = new int[10];

        // wypelnic tablice elementami, takimi ze w kazdym
        // indeksie jest wartosc + 20
        // 20 21 22 23 24 ...

        // petla wypelniajaca wartosciami
        for (int i = 0; i < tab.length; i++) {
            tab[i] = i * i;
        }

        //petla drukujaca wartosci
        for (int i = 0; i < tab.length; i++) {
            System.out.println("Indeks: " + i + " wartosc: " + tab[i]);
        }

        // policzyc sume elementow tablicy
        int suma = 0;
        for (int i = 0; i < tab.length; i++) {
            int elem = tab[i];
            suma += elem;
        }

        System.out.println("Suma elementow tablicy to: " + suma);

        // policzyc srednia elementow w tablicy
        System.out.println((double) suma / tab.length);


    }
}
