package Zajecia4;

/*      Napisz​ ​ program​ ​ wczytujący​ ​ ciąg​ ​ liczb​ ​ rzeczywistych.​
        Wydrukuj​ ​ na​ ​ ekranie​ ​ kolejno wszystkie​ ​ liczby,​ ​ które​
        należą​ ​ do​ ​ przedziału​ ​ [4;15). */

import java.util.Scanner;

public class RandomMathes {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj rozmiar tablicy: ");
        int rozmiar = sc.nextInt();
        int[] tablica = new int[rozmiar];

//        wpisywanie wartości przez użytkownika:
        for (int i = 0; i < tablica.length; i++) { //tyle ile tablica ma elementów, tyle razy prosimy o podanie liczby
            System.out.println("Podaj " + (i + 1) + "wartość: "); // do i-tego elementu tablicy wpisz wartość od użytkownika
            tablica[i] = sc.nextInt();
        }

//        wyświetlić wartości:
        // jeżeli wartość jest z przedziały od 4 do 15
        for (int i = 0; i < tablica.length; i++) { // wypisz tylko jeżeli jest w okreslonym przedziale
            if (tablica[i] >= 4 && tablica[i] < 15) {
                System.out.println(tablica[i] + " ");
                System.out.println("Indeks: " + i + ", wartosc: " + tablica[i]);
            }
        }

    }
}
