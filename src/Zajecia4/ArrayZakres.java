package Zajecia4;
/*      Napisz​ ​ program​ ​ wczytujący​ ​ z​ ​ klawiatury​ ​ n​ ​ liczb​ ​ całkowitych.​ ​ Liczbę​ ​ n​ ​ należy​ ​ pobrać​ ​ od użytkownika.​ ​
        Jeśli​ ​ podana​ ​ wartość​ ​ jest​ ​ z​ ​ zakresu​ ​ 1-30,​ ​ wówczas​ ​ należy​ ​ pobrać​ ​ podaną​ ​ ilość liczb​ ​ całkowitych,​ ​
        a​ następnie​ ​ wydrukować​ ​ każdą​ ​ z​ ​ liczb​ ​ podniesioną​ ​ do​ ​ kwadratu​ ​ na ekranie.​ ​ Jeśli​ ​ liczba​ ​ jest​ ​ spoza​ ​
        tego​ ​ przedziału​ ​ należy​ ​ zakończyć​ ​ pracę​ ​ drukując​ ​ stosowny komunikat. */

import java.util.Scanner;

public class ArrayZakres {
    public static void main(String[] args) {
//      deklaracja skanera
        Scanner sc = new Scanner(System.in);
//        utworzenie tablicy obiektów typu int o nazwie tab
//        wiadomość dla użytkownika

        System.out.println("Podaj wartość z zakresu 1 - 30");
        int rozmiar = sc.nextInt();
        if ( rozmiar > 0 && rozmiar <= 30){
            int[] tab = new int[rozmiar];
            for (int i = 0; i < tab.length; i++) {
                tab[i] = i * i;
            }
            System.out.println("Wartość tablicy-------");
            for(int i=0;i<tab.length;i++) {
                System.out.println(tab[i]);
            }
        }else{
            System.out.println("Nieprawidłowy zakres");
        }
    }
}
