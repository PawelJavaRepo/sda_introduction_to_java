package Zajecia7.zadanie6;

public class Worker extends Person { // subclass

    private int salary;
    private String position;

    public Worker(String name, String surname, int age, int salary, String position) {
        super(name, surname, age);
        this.salary = salary;
        this.position = position;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String describe() {
        return String.format("Mam na imie %s, pracuje w %s i zarabiam %d zlotych rocznie", getName(), getPosition(), getSalary());
    }

}
