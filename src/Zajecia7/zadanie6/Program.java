package Zajecia7.zadanie6;

public class Program {
    public static void main(String[] args) {
        Person person = new Person("Piotr", "Kowalski", 20);
        System.out.println(person.describe());

        Student student = new Student("Pawel", "Sydorowicz", 29, "nr: 24004", "Wyzsza Szkola Pedagogiki i Administracji");
        System.out.println(student.describe());

        Student studentInformatyki = new Student("Karol", "Parzonka", 29, "indeks nr. 23987", "Wyzsza Szkola Lansu i Bounceu");
        System.out.println(studentInformatyki.describe());

        System.out.println(studentInformatyki);

        Worker worker = new Worker("Jan", "Wawrzyniak", 48, 55564, "Miejskim przedsiebiorstwie oczyszczania sciekow");
        System.out.println(worker.describe());
    }
}
