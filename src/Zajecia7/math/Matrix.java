package zajecia7.math;

import java.util.Random;
import java.util.Scanner;

public class Matrix {
    private int[][] matrix;
    private int x;
    private int y;

    /**
     * Creates new matrix
     *
     * @param x - first dimension of matrix
     * @param y - second dimension of matrix
     */
    public Matrix(int x, int y) {
        this.x = x;
        this.y = y;
        matrix = new int[x][y];
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    /**
     * Adds second Matrix given as a parameter
     *
     * @param second
     * @return
     */
    public Matrix addMatrix(Matrix second) {
        //najpierw sprawdzic czy wymiary drugiej sie zgadzaja
//        if (this.x == second.x && this.y == second.y)
        if (this.x != second.x || this.y != second.y) {
            throw new ArithmeticException("Wymiary macierzy sie nie zgadzaja");
        }
        //przejsc do obliczen
        Matrix result = new Matrix(this.x, this.y);
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                result.matrix[i][j] = this.matrix[i][j] + second.matrix[i][j];
            }
        }
        return result;
    }

    public void print() {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    /**
     * Fills matrix with random values
     */
    public void fillWithRandomValues() {
        Random random = new Random();
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                matrix[i][j] = random.nextInt(50);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                stringBuilder.append(matrix[i][j] + " ");
            }
            stringBuilder.append('\n');
        }
        return stringBuilder.toString();
    }
}
