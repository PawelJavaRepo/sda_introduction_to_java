package Zajecia7.abstraction;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Figura[] tablicaFigur = new Figura[10];
        int ilosc = 0;
        boolean czyWyjsc = false;
        while (!czyWyjsc) {
            System.out.println("Dodaj nową figure:");
            System.out.println("1.  Kwadrat");
            System.out.println("2.  Prostokat");
            System.out.println("3.  Kolo");
            System.out.println("4.  Pola wszystkich");
            System.out.println("5.  Wyjscie");
            int wybor = sc.nextInt();
            Figura figura;
            switch (wybor) {
                case 1: {
                    System.out.println("Podaj bok kwadratu");
                    double bok = sc.nextDouble();
                    figura = new Kwadrat(bok);
                    tablicaFigur[ilosc++] = figura;
                    break;
                }
                case 2: {
                    System.out.println("Podaj boki prostokata");
                    double bok1 = sc.nextDouble();
                    double bok2 = sc.nextDouble();
                    figura = new Prostokat(bok1, bok2);
                    tablicaFigur[ilosc++] = figura;
                    break;
                }
                case 3: {
                    System.out.println("Podaj dlugosc promienia");
                    double promien = sc.nextDouble();
                    figura = new Kolo(promien);
                    tablicaFigur[ilosc++] = figura;
                    break;
                }
                case 4: {
                    System.out.println("Pola wszystkich:");
                    for (int i = 0; i < ilosc; i++) {
                        Figura f = tablicaFigur[i];
                        System.out.println("Pole: " + f.obliczPole());
                        System.out.println("Obwod: " + f.obliczObwod());
                    }
                    break;
                }
                case 5: {
                    czyWyjsc = true;
                    break;
                }
            }
        }
    }
}


