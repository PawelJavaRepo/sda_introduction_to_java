package Zajecia7.abstraction;

public class Kolo extends Figura {
    private double r;
    final private double pi = 3.14;

    public Kolo(double r){
        this.r = r;
    }

    @Override
    public double obliczPole() {
        return pi * r * r;
    }

    @Override
    public double obliczObwod() {
        return 2*pi*r;
    }
}
