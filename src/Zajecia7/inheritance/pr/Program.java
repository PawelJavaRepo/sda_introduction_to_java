package zajecia7.inheritance.pr;

import zajecia7.inheritance.Person;
import zajecia7.inheritance.Student;
import zajecia7.inheritance.Worker;

public class Program {
    public static void main(String[] args) {
        Person person = new Person("Piotr", "Kowalski", 20);
        System.out.println(person.describe());
//        System.out.println(person.getUniversity()); to nie zadziała - klasa person nie ma takiego pola

        Student student = new Student("Jan", "Nowacki", 25);
        System.out.println(student.describe());
        System.out.println(student.getUniversity());

        Student studentInformatyki = new Student("Kasia", "Pawlak", 22, "121212", "Politechnika");

        System.out.println(studentInformatyki.describe());

        System.out.println(person);
        System.out.println(studentInformatyki);

        Worker worker = new Worker("Jan", "Kowalski", 24, 2500, "asystent");
        System.out.println(worker.toString());
    }
}
