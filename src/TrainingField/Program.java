package TrainingField;

import java.util.Scanner;

public class Program extends Dzialania {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int wybor;

        Dzialania dzialania = new Dzialania();

        do {
            dzialania.menu();
            switch (wybor = sc.nextInt()) {
                case 1:
                    dzialania.dodawanie();
                    break;
                case 2:
                    dzialania.odejmowanie();
                    break;
                case 3:
                    dzialania.mnozenie();
                    break;
                case 4:
                    dzialania.dzielenie();
                    break;
                case 5:
                    dzialania.pierwiastek();
                    break;
                case 6:
                    dzialania.potegaKwadratowa();
                    break;
                case 7:
                    dzialania.percent();
                    break;
                case 8:
                    dzialania.wyjscie();
            }
        } while (wybor < 8);
    }
}

