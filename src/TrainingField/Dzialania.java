package TrainingField;

import java.util.Scanner;

public class Dzialania implements Calculator {

    private double a;
    private double b;
    private double wynik;

    public Dzialania() {}

    @Override
    public void menu(){
        System.out.println("Wybierz jedną z opcji MENU:");
        System.out.println("");
        System.out.println("1.  A + B");
        System.out.println("2.  A - B");
        System.out.println("3.  A * B");
        System.out.println("4.  A / B");
        System.out.println("5.  Pierwiastek kwadratowy z A");
        System.out.println("6.  A do potegi B");
        System.out.println("7.  Procenty");
        System.out.println("8.  Wyjscie");
    }

    @Override
    public void dodawanie() {
        numbers();
        wynik = a + b;
        System.out.println(wynik);
    }

    @Override
    public void odejmowanie() {
        numbers();
        wynik = a - b;
        System.out.println(wynik);
    }

    @Override
    public void mnozenie() {
        numbers();
        if (a == 0 || b == 0) {
            throw new IllegalArgumentException("Nie wolno mnozyc przez zero");
        }
        wynik = a * b;
        System.out.println(wynik);
    }

    @Override
    public void dzielenie() {
        numbers();
        if (a == 0 || b == 0) {
            throw new IllegalArgumentException("Nie wolno dzielic przez zero");
        }
        wynik = a / b;
        System.out.println(wynik);
    }

    @Override
    public void pierwiastek() {
        numbers();
        if (a == 0) {
            throw new IllegalArgumentException("Liczba nie może być rowna zero");
        }
        wynik = Math.sqrt(a);
        System.out.println(wynik);
    }

    @Override
    public void potegaKwadratowa() {
        numbers();
        if (a == 0) {
            throw new IllegalArgumentException("Nie można podniesc zera do kwadratu");
        }
        if (b == 0) {
            throw new IllegalArgumentException("Nie ma pierwiastka zerowego");
        }
        wynik = Math.pow(a, b);
        System.out.println(wynik);
    }

    @Override
    public void percent() {
        numbers();
        wynik = a * (b * 1 / 100);
        System.out.println(wynik);
    }

    public void numbers() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj A:");
        a = sc.nextDouble();
        System.out.println("Podaj B:");
        b = sc.nextDouble();
    }

    public void wyjscie(){
        System.out.println("Do widzenia ;)");
    }
}