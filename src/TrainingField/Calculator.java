package TrainingField;

public interface Calculator {

//    dodawanie
//    odejmowanie
//    mnozenie
//    dzielenie
//    pierwiastek
//    potega kwadratowa
//    potega szescienna
//    ile procent

    void menu();

    void dodawanie();

    void odejmowanie();

    void mnozenie();

    void dzielenie();

    void pierwiastek();

    void potegaKwadratowa();

    void percent();
}
