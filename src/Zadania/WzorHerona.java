package Zadania;

/*          Wpisz ​ długości​ ​ trzech​ ​ boków trójkąta​.​
            Sprawdź​ ​ czy​ ​ można​ ​ z​ ​ nich​ ​ zbudować​ ​ trójkąt,​
            jeżeli​ ​ tak to​ ​ oblicz​ ​ jego​ ​ pole
            oraz sprawdz, czy jest to trójkąt prostokątny.*/

import java.util.Scanner;

public class WzorHerona {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

// todo: wczytać długość boków

        System.out.println("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        System.out.println("PROGRAM TEN MA NA CELU SPRAWDZIĆ CZY DŁUGOŚCI BOKU JAKIE WPROWADZASZ MOGĄ UTWORZYĆ TRÓJKĄT,");
        System.out.println("A JEŻELI TAK, OBLICZĄ JEGO POLE I SPRAWDZĄ CZY JEST PROSTOKĄTNY.");
        System.out.println("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        System.out.println("Wprowadź długość boku 'A'");
        float boka = sc.nextFloat();
        System.out.println("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        System.out.println("Wprowadź długość boku 'B'");
        float bokb = sc.nextFloat();
        System.out.println("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        System.out.println("Wprowadz długość boku 'C'");
        float bokc = sc.nextFloat();
        System.out.println("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");

        if ((boka + bokb > bokc) && (bokb + bokc > boka) && (bokc + boka > bokb)) {
            System.out.println("Z podanej długości boków można utworzyć trójkąt");
            double p = (boka + bokb + bokc) / 2;
            double heron = p * (p - boka) * (p - bokb) * (p - bokc);
            double pole = Math.sqrt(heron);
            System.out.println(String.format("Pole wynosi: %3f", pole));

            System.out.println("Sprawdzam czy trójkąt jest 'prostokątny'.");

            if ((bokc * bokc) == ((boka * boka) + (bokb * bokb))) {
                System.out.println("Tak. Ten trójkąt jest trójkątem prostokątnym.");
            } else {
                System.out.println("Niestety, ten trójkąt nie jest trójkątem prostokątnym.");
            }
        }


    }
}
