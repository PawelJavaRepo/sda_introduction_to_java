package Zadania;

/*  Utwórz​ ​ zmienne​ ​ następujących​ ​ typów:
    typ​ ​ całkowitoliczbowy​ ​ (3​ ​ rodzaje),
    typ​ ​ zmiennoprzecinkowy(2​ ​ rodzaje),
    pojedynczy​ ​ znak, łańcuch​ ​ znaków,
​ ​   wartość​ ​ prawda/fałsz Wypisz​ ​ te​ ​ wartości​ ​ na​ ​ konsolę */

public class s27 {
    public static void main(String[] args) {

//        typ całkowity:
        byte pierwszy;
        short drugi;
        int trzeci;
        long czwarty;

//        zmiennoprzecinkowy: 2 rodzaje
        float a;
        double b;

//        pojedynczy znak
        char c;

//        lancuch znakow
        String d;

//        prawda/falsz
        boolean e;

//        inicjalizacja zmiennych

        pierwszy = 1;
        drugi = 32000;
        trzeci = -2140150999;
        czwarty = 3128658785L;
        a = 10.12345F;
        b = 120.123456789D;
        c = '@';
        d = "PawelSydorowicz";
        e = true;

//        Wypisanie zmiennych na konsoli;

        System.out.println(pierwszy);
        System.out.println(drugi);
        System.out.println(trzeci);
        System.out.println(czwarty);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
        System.out.println(e);
    }
}
