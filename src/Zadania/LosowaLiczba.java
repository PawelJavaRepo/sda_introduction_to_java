package Zadania;

import java.util.Random;
import java.util.Scanner;

/*      Program w którym użytkownik zgaduje losową liczbę z zakresu od 01 do 10 wybraną przez komputer.         */

public class LosowaLiczba {
    public static void main(String[] args) {

        Random r = new Random();                    //utworzenie obiektu klasy Random do losowania liczb z danego zakresu.
        Scanner odczyt = new Scanner(System.in);    //utworzenie obiektu klasy Scanner, aby użytkownik mógł typować liczbę

        int a = r.nextInt(11);              //Polecenie dla komputera aby wylosował liczbę z zakresu.

        System.out.println("Podaj liczbę całkowitą z zakresu od 0 do 10:");

        int b = odczyt.nextInt();                   //użytkownik wpisuje swój typ.

        if (a == b) {                               //warunek "jeśli a będzie równe b.
            System.out.println("Brawo, udało Ci się odgadnąć jaką liczbę wylosował komputer ;)");
        } else {
            System.out.println("Spróbuj ponownie.");
        }                                           //koniec if/else
    }                                               //koniec main
}                                                   //koniec klasy
