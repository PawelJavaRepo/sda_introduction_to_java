package Zadania.Pętle;

public class FromOneToHundred {
    public static void main(String[] args) {

        /*      Program wypisujący cyfry od 1 do 100        */

        for (int a=0; a<101; a++) { // inicjalizacja pętli for.
            System.out.println(a + " pętla FOR");
        }

        int b = 0;
        while (b < 101) {
            System.out.println(b + " pętla WHILE");
            b++;
        }
    }
}
