package Zadania;

import java.util.Scanner;

public class NierównoscTrojkata {
    public static void main(String[] args) {

        double a;
        double b;
        double c;

        Scanner odczyt = new Scanner(System.in);

        System.out.println("Wpisz długość boku A:");
        a = odczyt.nextDouble();
        System.out.println("Wpisz długość boku B:");
        b = odczyt.nextDouble();
        System.out.println("Wpisz długość boku C:");
        c = odczyt.nextDouble();

        if (a < (b + c) && b < (a + c) && c < (a + b)) {
            System.out.println("Warunek Nierówności Trójkąta został zachowany.");
        } else {
            System.out.println("Niestety, nic z tego ;(");
        }
    }
}
