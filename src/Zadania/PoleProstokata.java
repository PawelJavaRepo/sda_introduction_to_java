package Zadania;

/*Napisz​ ​ program,​ ​ który​ ​ oblicza​ ​ pole​ ​ prostokąta.​ ​
Wartości​ ​ boków a​ ​ i​ ​ b​ ​ wprowadzamy​ ​ z​ ​ klawiatury.​ ​
W​ ​ programie​ ​ należy​ ​ przyjąć, że​ ​ zmienne​ ​ a,​ ​ b​ ​ oraz​ ​ pole​ ​ są​ ​ typu​ ​ double​ ​ (rzeczywistego).

 */

import java.util.Scanner;

public class PoleProstokata {
    public static void main(String[] args) {


//    inicjalizacja skanera

        Scanner odczyt = new Scanner(System.in);

//    deklaracja zmiennych:
        double a;
        double b;

//    Wykonanie programu:

        System.out.println("Wprowadz długość boku a:");
        a = odczyt.nextDouble();
        System.out.println("Wprowadz długość boku b:");
        b = odczyt.nextDouble();
        double pole = (2 * a) + (2 * b);
        System.out.println("Pole prostokąta o bokach: " + a + "cm, " + b + "cm wynosi: " + pole + "cm.");


    }
}
