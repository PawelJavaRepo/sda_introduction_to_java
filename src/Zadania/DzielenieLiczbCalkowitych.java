package Zadania;

/*Napisz​ ​ program,​ ​ który​ ​ oblicza​ ​ wynik​ ​ dzielenia​ ​
całkowitego bez​ ​ reszty​ ​ dwóch​ ​ liczb​ ​ całkowitych:​ ​
 a​ ​ =​ ​ 37​ ​ i​ ​ b​ ​ =​ ​ 11. */

public class DzielenieLiczbCalkowitych {

    public static void main(String[] args) {

//        deklaracja zmiennych
        int a;
        int b;
        int c;

//        inicjalizacja zmiennych
        a = 37;
        b = 11;
        c = a / b;

//        Wydruk w konsoli

        System.out.println("Wynik dzielenia całkowitego z liczb " + a + " oraz " + b + " wynosi " + c + ".");


    }
}
