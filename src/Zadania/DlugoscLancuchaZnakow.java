package Zadania;

import java.util.Scanner;

/*          Program zliczający długość łańcucha znaków.         */
//                    Bez białych znaków:
public class DlugoscLancuchaZnakow {
    public static void main(String[] args) {

        Scanner odczyt = new Scanner(System.in);
        String a;
        System.out.println("Poniżej wprowadź dowolny tekst, ale bez SPACJI. Po wciśnięciu klawisza ENTER, program obliczy z ilu znaków się składa.");
        a = odczyt.next();
        System.out.println(a.length());

//        Z białymi znakami:
        String b;
        System.out.println("A teraz wpisz coś używając klawisza SPACJA.");
        b = odczyt.next();

    }
}
