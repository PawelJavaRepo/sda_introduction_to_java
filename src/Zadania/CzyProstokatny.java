package Zadania;

import java.util.Scanner;

public class CzyProstokatny {

    public static void main(String[] args) {

        Scanner odczyt = new Scanner(System.in);

        double a;
        double b;
        double c;

        System.out.println("Wprowadz długość boku a:");
        a = odczyt.nextDouble();
        System.out.println("Wprowadz długość boku b:");
        b = odczyt.nextDouble();
        System.out.println("Wprowadz długosc boku c:");
        c = odczyt.nextDouble();

        if ((a * a) + (b * b) == (c * c)) {
            System.out.println("Ten trójkąt jest prostokątny.");
        } else {
            System.out.println("Niestety ten trójkąt nie jest prostokątny.");
        }
    }
}
