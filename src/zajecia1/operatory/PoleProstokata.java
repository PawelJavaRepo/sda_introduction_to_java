package zajecia1.operatory;

import java.util.Scanner;

/*
    Przyklad obliczania pola prostokata dla danych wartosci
 */
public class PoleProstokata {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj boki prostokata: ");
        double a = sc.nextDouble();
        double b = sc.nextDouble();

        // policz pole i wypisz
        double pole = a * b;

        //policz obwod i wypisz
        double obwod = 2 * a + 2 * b;

        System.out.println(obwod);

        double duzePole = 100;
        boolean czyWieksze = pole > duzePole;

        String imie;
        imie = "Piotr";
        String napis = "Dzien dobry";

        System.out.println("Czy duze pole?" + czyWieksze);

        //alternatywny zapis bez tworzenia dodatkowej zmiennej
        System.out.println("Czy duze pole?" + (pole > duzePole));
    }

}
