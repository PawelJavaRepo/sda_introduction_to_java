package zajecia1.operatory;

public class OperatoryPorownawcze {

    public static void main(String[] args) {
        int a = 15;
        int b = 20;

        System.out.println("A = " + a + " B = " + b);

        // do zmiennej boolean o nazwie czyRowne wpisz (operator =  )
        //wynik operacji porownania a == b
        boolean czyRowne = a == b;
        System.out.println("Czy a wieksze od b?" + czyRowne);

        //czy rozne
        boolean czyRozne = a != b;
        System.out.println("Czy a rozne od b?" + czyRozne);

        //czy a wieksze od b?
        boolean czyWieksze = a > b;
        System.out.println("Czy a wieksze od b? " + czyWieksze);

        //czy a mniejsze od b?
        boolean czyMniejsze = a < b;
        System.out.println("Czy a mniejsze od b? " + czyMniejsze);
    }
}
