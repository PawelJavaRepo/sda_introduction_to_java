package zajecia1.NewClass;

public class NewClass {

    public static void main(String[] args) {
        System.out.println("Agata");
        System.out.println("Bartek");
        System.out.println("Kasia");

        int x = 3;
        int z = 45;


        //  ctrl+ slash automatycznie zakomentowuje linie
        /*  ctrl + Shift + slash zakomentowuje zaznaczony obszar tekstu*/

        /*  ZMIENNA - przechowuje dane różnych typów

        1. typy liczb całkowitych:

        byte   - 1 bajt    -   (od -128 do 127 czyli 256 wartości);
        short  - 2 bajty   -   (od -32 768 do 32 767 czyli 65 000 wartości);
        int    - 4 bajty   -   (od -2 147 483 648 do 2 147 483 647)
        long   - 8 bajtów  -   (od -

        2. typy liczb zmiennoprzecinkowych:

        float  - 4 bajty   -   (max około 6-7 cyfr po przecinku)
        double - 8 bajtów  -   (max około 15 cyfr po przecinku)

        3. zmienne typu char (KAR):

        zmienne typu pojedyńczego znaku. Zapisujemy je w APOSTROFIE ('), może przechowywać dowolny znak lub literę lub
        liczbę całkowitą do 2 bajtów

        4. boolean:

        może przyjmować dwie wartości TRUE lub FALSE*/

        System.out.println(); //    ctrl+p wywołuje podpowiedź
//                                  alt + Insert - otwiera menu "nowy"

        int wiek;
        int wzrost;
        byte pierwszaLiczba;
        short drugaLiczba;
        int trzeciaLiczba;
        long czwartaLiczba;
        float pi = 3.14f;
        double sri = 10.189;
        wiek = 29;
        wzrost = 184;
        pierwszaLiczba = 7;
        drugaLiczba = 30000;
        trzeciaLiczba = 1700123;
        czwartaLiczba = 123456789;
        System.out.println(wiek);
        System.out.println(wzrost);
        System.out.println(pierwszaLiczba);
        System.out.println(drugaLiczba);
        System.out.println(trzeciaLiczba);
        System.out.println(czwartaLiczba);
        System.out.println(pi);
        System.out.println(sri);

        System.out.println(wiek + " " + sri);

        // zapamiętać:
        // DEKLARACJA I INICJALIZACJA
        // typy prymitywne: byte, short, int, long, char, float, double, boolean.
        // typy obiektowe-referencyjne-złożone: String

        String janusz;
        janusz = "Janusz";
        System.out.println(janusz);

        boolean czyZrobiszMiKawe = true;

        System.out.println("czy Zrobisz Mi Kawe?");

        if (czyZrobiszMiKawe) {
            System.out.println("Oczywiście, że tak");
        }else{
            System.out.println("Sam sobie zrób");

            /* stałe: raz zadeklarowana stała nie ma prawa zmienić wartości.
//                final int liczbaPi = 3.14;*/

        }
    }


}